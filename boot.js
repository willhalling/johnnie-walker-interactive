define( [], function () {

  'use strict';

  // Get paths for assets (css + js)
  
  //var rootPath = "https://labs.theguardian.com/2016/johnniewalker/jwv2/";
  var rootPath = "http://localhost:5000/_build/";
  
  function addCSS( url ) {
    var head = document.querySelector( 'head' );
    var link = document.createElement( 'link' );
    link.setAttribute( 'rel', 'stylesheet' );
    link.setAttribute( 'type', 'text/css' );
    link.setAttribute( 'href', url );
    head.appendChild( link );
  }

  return {
    boot: function ( el, context, config, mediator ) {
      // Load CSS
      addCSS( 'https://interactive.guim.co.uk/next-gen/labs-boot/ng-interactive/webfonts/webfonts.css' );
      addCSS( rootPath + 'assets/css/bootstrap.css' );

      // Load main application
      require( [ rootPath + 'assets/js/script.js'], function ( req ) {
        // Main app returns a almond instance of require to avoid
        // R2 / NGW inconsistencies.
        req( ['main'], function ( main ) {

          main.init( el, rootPath );

        } );
      }, function ( err ) {
        console.error( 'Error loading boot.', err );
      } );
    }
  };
} );
