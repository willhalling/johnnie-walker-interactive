define(function () {

    function controllerBase() {
        self.quotes = null;
    }

    controllerBase.prototype = {

        setData: function(data, root) {
          this.data = data;
          console.log('data' + data);
          this.init(root);
        },

        socialLinks: function () {
            var self = this;

            function getLinkID(id){
              var id = parseInt(id-1);
              return id;
            }

            $( ".facebook a" ).click(function() {
              var id = $(this).closest("section").attr("id").match(/\d+$/)[0];
              var id = getLinkID(id);
              self.shareFacebook(self.quotes[id].title);
            });

            $( ".twitter a" ).click(function() {
              self.shareTwitter();
            });

            $( ".pininterest a" ).click(function() {
              self.sharePininterest();
            });
        },


        shareFacebook: function(title) {
  
          var self = this;

          var facebook = {
            "display": "popup",
            "app_id": "741666719251986",
            "link": "http://www.theguardian.com",
            //"picture": "india.jpg",
            "redirect_uri": "http://www.theguardian.com/interactive",
            "name": "Mind your manners",
            "caption": "Johnnie Walker",
            "description": "*** Quote description goes here. ***"
          };

          var base = "https://www.facebook.com/dialog/feed",
          params = facebook;

          // commented out for localhost
          //params.link = window.location.href;
          params.link = facebook.link;

          params.name = 'Johnnie Walker: ' + title;

          // commented out for localhost
          //params.picture = this.rootPath + 'images/' + facebook.picture;

          this.openShareWindow(self.createUrl(base, params), 520, 350);

        },

        sharePininterest: function() {
          
          var self = this;

          var pininterest = {
            "media": "http://tinyurl.com/p3rqf54",
            "description": "*** Pininterest description goes here. ***",
            "url": "http://www.theguardian.com",
          }

          var base = 'http://pinterest.com/pin/create/button',
              params = pininterest;

          // commented out for localhost
          // params.url = window.location.href;
          params.url = pininterest.url;
          
          this.openShareWindow(self.createUrl(base, params), 520, 350);

        },

        shareTwitter: function() {

          var self = this;

          var twitter = {
            "text": "*** Twitter text goes here. ***",
            "source": "http://www.theguardian.com"
          }

          var base = 'https://twitter.com/intent/tweet',
              params = twitter;

          params.text += ' ' + window.location.href;
          this.openShareWindow(self.createUrl(base, params), 520, 350);
        },

        openShareWindow: function(url, winWidth, winHeight) {
            var winTop = (screen.height / 2) - (winHeight / 2);
            var winLeft = (screen.width / 2) - (winWidth / 2);
            window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
        },

        createUrl: function(base, paramsObj) {

            var params = [];

            Object.keys(paramsObj).forEach(function(key) { 
              params.push(key + '=' + encodeURIComponent(paramsObj[key]));
            });

            var url = base + '?' + params.join('&');
            return url;

        },

        wayPoints: function (resultsLength, quoteContentHeights) {

            // hide quote content on load
            $('.quote').removeClass('active');
            
            //console.log('resultsLength: ' + resultsLength);
            //console.log('quoteContentHeights' + quoteContentHeights)

            var i;

            // if mobile show content/arrow when above quote content
            if ($("#mq-detector > span.visible-xs").is(":visible")) {                                                   
              var offset = 100;
            // if desktop show content/arrow when above quote content
            } else {
              var offset = 175;
            }

            function getQuoteHeights(number) {
              var totalHeight = 0;
              for(i=1; i < number; i++){
                totalHeight += quoteContentHeights[i];
              }
              return totalHeight;
            }

            for(i=1; i < resultsLength+1; i++){

              // get heights of open .quote-content
              var offsetHeight = getQuoteHeights(i);

              // first offset can't be nagative
              if (i == 1) {
                offsetHeight =  Math.abs(offsetHeight-offset)        
              } else {
                offsetHeight =  -Math.abs(offsetHeight-offset)   
              }

              var waypoint = new Waypoint({
                element: document.getElementById('quote-'+i+''),
                handler: function(direction) {
                  if (direction == "down") {
                    //console.log(this.element.id + ' triggers at ' + this.triggerPoint)
                    $("."+this.element.id+"").addClass('active');
                    //console.log('this.element.id ' + this.element.id);
                  }
                },
                triggerOnce: true,
                offset: offsetHeight
              });

              $(window).scroll(function(){   
                  var st=$(window).scrollTop();
                  var offsetStart = 175;
                  $('.quote-image-after').each(function(index) {
                      var o = (Math.min(($(this).offset().top - st) / offsetStart, 0.75));
                      $(this).css({ 'opacity' : o });
                  });
              });
            }

        },

        init: function(url) {

            var self = this;
            //var data = ''+url+'data/quotes.json';
            var data = 'https://labs.theguardian.com/2016/johnniewalker/jwv2/data/quotes.json';

            var quoteContentHeights = [];
            function splitWords(words) {
              //$('.hero_image p').each(function(){
                var text = words.split(' '),
                    len = text.length,
                    result = []; 

                for( var i = 0; i < len; i++ ) {
                    result[i] = '<span>' + text[i] + '</span>';
                }
                //$(this).html(result.join(' '));
                var result = result.join(' ');
                return result;
            }

            self.data.loadData(data,
                function(result) { 
                 console.log(JSON.stringify(result));
                 // save quotes globally
                 self.quotes = result;          
                  for(var i = 0; i < result.length; i++) {
                      var title = splitWords(result[i].title);
                      var number = i+1;
                      $('#quotes').append('<section id="quote-'+number+'" class="quote active quote-'+number+'">\
                        <div class="quote-image">\
                          <div class="quote-circle text-center">\
                              <div class="quote-numbers"><strong> '+number+'</strong><span class="quote-numbers-fade">/21</span></div>\
                              <div class="quote-circle-inner">\
                                <h2 class="quote-title text-center">\
                                  '+title+'\
                                </h2>\
                              </div>\
                          </div>\
                          <div class="quote-image-container" style="background-image: url(\''+url+'assets/img/mobile/'+number+'-mobile.jpg\');">\
                            <img class="hero-img hidden-xs" src="'+url+'assets/img/'+number+'.jpg" />\
                            <div class="arrow-up"></div>\
                          </div>\
                          <div class="quote-image-after"></div>\
                        </div>\
                        <div class="quote-content">\
                          <div class="quote-text text-center"><p>'+result[i].content+'</p></div>\
                          <ul class="quote-share text-center list-unstyled">\
                            <li class="pininterest"><a href="javascript:void(0);"></a></li>\
                            <li class="facebook"><a href="javascript:void(0);"></a></li>\
                            <li class="twitter"><a href="javascript:void(0);"></a></li>\
                          </ul>\
                          <p class="quote-footer text-center">Share this quote</p>\
                        </div>\
                      </section>\
                      ');
                    
                      // need to get all heights to use in waypoint plugin
                      var height = $('.quote-'+number+' .quote-content').height();
                      quoteContentHeights.push(height);
                  }
                  self.wayPoints(result.length, quoteContentHeights);
                  self.socialLinks();
                },
                function(xhr) { 
                  console.error(xhr); 
                }
              ); 
        }



    };

    return controllerBase;
});