require.config({
	baseUrl: 'assets/js',
	paths: {
		'app': 'app',
		'data': 'data',
		'quotes': 'quotes',
		'waypoints': './waypoints/lib/noframework.waypoints',
		'jquery': './jquery-2.1.0.min/index'
	}
});

define(function (require) {

    var waypoints = require('waypoints'),
    	$ = require('jquery'),
        model = require('./model'),
        controller = require('./controller');

    function setData(root) {
    	controller.setData(model, root);
    };

    function init(el, root) {

        el.innerHTML = '\
			<div id="start" class="wrap">\
				<div class="hero">\
				    <div class="hero-text">\
				      <h1 class="text-center">21 ways to<br />find joy in<br />everyday life</h1>\
				    </div>\
				    <a class="logo" href="#"></a>\
				    <div class="circle"></div>\
				    <div class="line"></div>\
				    <div class="line2"></div>\
				</div>\
				<div id="quotes"></div>\
			</div>\
			<span id="mq-detector">\
				<span class="visible-xs"></span>\
				<span class="visible-sm"></span>\
				<span class="visible-md"></span>\
				<span class="visible-lg"></span>\
			</span>';

	    setData(root);

    }

    return {
        init: init
    };

});