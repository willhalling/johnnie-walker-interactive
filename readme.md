# Interactive - Johnnie Walker
# Dev Prerequisites
- [Node](https://nodejs.org/ "Node") v.4.1.1 installed  (to manage node versions use [NVM](https://github.com/creationix/nvm) )
- [NPM](https://www.npmjs.com/ "NPM Package Manager") installed
- [REQUIREJS](http://requirejs.org// "Require JS") installed

### Setup/development
- Clone this repository and change dir to folder
- Run the following commands:
- `npm install`
- `bower install`
- `grunt build`
- Files are copied to `_build` folder
- run local server on port 5000 (e.g.  python -m SimpleHTTPServer 5000)
- preview/development at http://interactive.guim.co.uk/preview/#http://localhost:5000/_build/boot.js
- uncomment @path variable in johnie-walker.less depending on environment.

### General Notes on Updating Quotes/Photos
##### Quote Photographs
- Desktop photos min 788 x 450 px (use 1,576 x 900 px to cater for retina). Upload to assets/img/ in X.jpg format.
- Mobile photos min 375 x 377 px (use 750 x 750 px to cater for retina). Upload to assets/img/mobile in X.jpg format.

##### Social Media Photographs
- Facebook, 1200 x 630 px, link: https://developers.facebook.com/docs/sharing/best-practices
- Twitter, 280 x 150 px, link: https://dev.twitter.com/cards/types/summary-large-image
- Pininterest, 100 x 200 px, link: https://developers.pinterest.com/docs/widgets/pin-it/

##### Updating Social Media Quotes
- To update specific share text look inside /data/quotes.json
- TO DO: need to integrate json data inside share buttons. Look at Facebook title for example inside assets/js/quotes.js