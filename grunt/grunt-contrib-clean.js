module.exports = function(grunt) {

  grunt.config('clean', {
      build: ["_build/"],
      guardian: ["_build-guardian/"]
  });

  grunt.loadNpmTasks('grunt-contrib-clean');

};