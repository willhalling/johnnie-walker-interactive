module.exports = function(grunt) {

  grunt.config('processhtml', {
    options: {
      strip:true
    },
    main: {
      options: {
          data:{
              classes:"enel-body",
          }
      },
      files: {
          '_build/index.html': ['index_TEMPLATE.html']
      }
    },
    guardian: {
      options: {
          data:{
              classes:"enel-body",
          }
      },
      files: {
          '_build-guardian/index.html': ['index_TEMPLATE_Guardian.html']
      }
    }
  });
  grunt.loadNpmTasks('grunt-processhtml');
};