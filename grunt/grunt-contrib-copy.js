module.exports = function(grunt) {

  grunt.config('copy', {
    build: {
      files: [
        {expand: false, flatten: true, src: ['assets/**/*', '!**/js/**', 'assets/js/requirejs/require.js' ], dest: '_build/'},
        {expand: true, flatten: true, src: ['assets/css/bootstrap.css'], dest: '_build/assets/css/', filter: 'isFile'},
        {expand: true, flatten: true, src: ['node_modules/bootstrap/dist/js/bootstrap.min.js'], dest: '_build/assets/js/lib/', filter: 'isFile'},
        {expand: true, flatten: true, src: ['boot.js'], dest: '_build/', filter: 'isFile'},
        {expand: true, src: ['data/**/*'], dest: '_build/'}
      ],
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');

};