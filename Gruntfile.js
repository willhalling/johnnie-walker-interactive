module.exports = function(grunt) {

  var isDev = false;

  // initialise le config
  grunt.initConfig({
    pkg: require('./package.json'),

    bowerRequirejs: {
        all: {
            rjsConfig: './src/js/require.js',
            options: {
                baseUrl: './src/js/'
            }
        }
    },

    less: {
      build: {
        options: {
          paths: ["assets/css"]
        },
        files: {
          "assets/css/bootstrap.css": "assets/less/johnnie-walker.less"
        }
      }
    },

    requirejs: {
      compile: {
        options: {
          baseUrl: './assets/js/',
          mainConfigFile: './assets/js/main.js',
          optimize: (isDev) ? 'none' : 'uglify2',
          uglify2: {
            compress: {
              drop_debugger: true,
              drop_console: true,
              unused: true,
              dead_code: true
            },
            warnings: false,
            mangle: true
          },
          inlineText: true,
          name: 'almond/almond',
          out: '_build/assets/js/script.js',
          generateSourceMaps: true,
          preserveLicenseComments: false,
          include: ['main'],
          wrap: {
            start: 'define(["require"],function(require){var req=(function(){',
            end: 'return require; }()); return req; });'
          }

        }
      }
    },

  });

  // load per task config from separate files
  grunt.loadTasks('grunt');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-bower-requirejs');
  grunt.loadNpmTasks('grunt-contrib-less');

  // stand alone version
  grunt.registerTask('build', 'Run this before deploy',
    [
      'clean:build',
      'less',
      //'bowerRequirejs',
      'requirejs',
      'copy:build',
      'processhtml'
    ]
  );

  // guardian version
  grunt.registerTask('build_guardian', 'Run this before deploy',
    [
      'clean:guardian',
      'copy:guardian',
      'processhtml:guardian'
    ]
  );  

  grunt.registerTask('default', ['dev']);
  
  grunt.registerTask('serve', ['connect', 'watch']);

};
